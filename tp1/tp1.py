import scipy.linalg as nla
import matplotlib.pyplot as plt
import autograd as ag
import autograd.numpy as np

### Moindres carrés linéaires
print('I-MOINDRES CARRES LINEAIRES\n')

## Question 1
print('Question 1.\n')

alpha, beta, gamma, mu = 0.5, -2, 1, 7
m = 4
print('Solution initiale :')
print('alpha = %f, beta = %f, gamma = %f, mu = %f' %(alpha, beta, gamma, mu))

# On implémente la fonction f
def f(x):
    return alpha*x**3 + beta*x**2 + gamma*x + mu

# On place les points expérimentaux
Tx = np.array ([-1.1, 0.17, 1.22, -.5, 2.02, 1.81])
p = Tx.shape[0]
Ty_sur_la_courbe = np.array([f(x) for x in Tx])
perturbations = 0.5*np.array ([-1.3, 2.7, -5, 0, 1.4, 6])
Ty_experimentaux = Ty_sur_la_courbe + perturbations

# Calcul de l'erreur
erreur_initiale = np.linalg.norm(Ty_sur_la_courbe - Ty_experimentaux)
print('\nErreur initiale :')
print('e = %f' %erreur_initiale)

# Tracé de la courbe initiale
plt.scatter(Tx, Ty_sur_la_courbe)
xplot = np.linspace(-1.2, 2.1, 50)
yplot = np.array([f(x) for x in xplot])
plt.plot(xplot, yplot)
#plt.show()

# Calcul de la solution optimale avec QR
b = Ty_experimentaux
A = np.array([[x**3, x**2, x, 1] for x in Tx], dtype = np.float64)

Q1, R1 = nla.qr(A, mode='economic')
Q1Tb = np.dot(np.transpose(Q1), b)
alpha, beta, gamma, mu = nla.solve_triangular(R1, Q1Tb, lower = False)
print('\nSolution optimale calculée :')
print('alpha = %f, beta = %f, gamma = %f, mu = %f' %(alpha, beta, gamma, mu))

# On recalcule les Ty pour ensuite recalculer l'erreur
new_Ty_sur_la_courbe = np.array([f(x) for x in Tx])
erreur_minimale = np.linalg.norm(new_Ty_sur_la_courbe - Ty_experimentaux)
print('\nErreur minimale :')
print('e = %f' %erreur_minimale)

# On trace la courbe optimale (en orange) superposée à la courbe initiale (en bleu)
plt.scatter(Tx, Ty_sur_la_courbe, color = 'blue')
plt.scatter(Tx, new_Ty_sur_la_courbe, color = 'orange')
xplot = np.linspace(-1.2, 2.1, 50)
yplot2 = np.array([f(x) for x in xplot])
plt.title('Tracé des courbes optimales et initiales')
plt.plot(xplot, yplot, color = 'blue', label = 'Solution initiale')
plt.plot(xplot, yplot2, color = 'orange', label = 'Solution optimale')
plt.legend()
plt.show()



### Méthode de Newton en une variable
print('\nII-METHODE DE NEWTON EN UNE VARIABLE\n')

## Question 2
print('Question 2.\n')

# On implémente une fonction g tel que racine cubique de 2 soit racine
def g(x):
    return x**3 - 2

# On dérive la fonction g
def gprime(x):
    return 3*x**2

# Implémentation de la suite u qui converge vers racine cubique de 2.
u = 2
for n in range(0, 6):
    print ('u[%d] =' %n, u)
    u = u - g(u)/gprime(u)


# Question 3
print('\nQuestion 3.\n')

# On dérive la fonction f initiale
def fprime(x):
    return 3*alpha*x**2 + 2*beta*x + gamma

def fseconde(x):
    return 6*alpha*x + 2*beta

u = 2
for n in range(10):
    print ('u[%d] =' %n, u)
    u = u - fprime(u)/fseconde(u)


# Question 4
print('\nQuestion 4.\n')

fprime = ag.grad(f)
fesconde = ag.grad(fprime)

u = np.float64(2)
for n in range(10):
    print ('u[%d] =' %n, u)
    u = u - fprime(u)/fseconde(u)



