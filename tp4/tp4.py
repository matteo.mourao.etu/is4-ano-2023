import autograd as ag
import autograd.numpy as np
import matplotlib.pyplot as plt

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5


## Question 4
print('Question 4.\n')

(0.2255)**2 + (0.9312)**2

print('(0.2255)**2 + (0.9312)**2 = 0.91798369 > 0.5 donc ne vérifie pas la contrainte (3).\n')

print('La contrainte est réalisable donc est active. On peut ainsi transformer la contrainte..\n')

def c (a,b):
    return a**2 + b**2 - 1/2


## Question 5
print('Question 5.\n')

print('On calcule a à partir de la contrainte (4) : on obtient a = 0.685, et par suite b = ')


## Question 6
print('Question 6.\n')
def nabla_c (a,b) :
    return np.array ([2*a, 2*b], dtype=np.float64)


### Calcul du minimum
print('1.2.2 Calcul du minimum\n')

## Question 7
print('Question 7.\n')

print('Les variables du Lagrangien sont a, b et lambda\n')


## Question 8
print('Question 8.\n')

def Lagrangien (u):
    a, b, lamb = u
    return f(a,b) - lamb*c(a,b)


## Question 9
print('Question 9.\n')

nabla_Lagrangien = ag.grad(Lagrangien)
H_Lagrangien = ag.hessian(Lagrangien)
u = np.array([0.22, 0.685, 0], dtype=np.float64)


## Question 10
print('Question 10.\n')

for n in range(6):
    H = H_Lagrangien(u)
    g = nabla_Lagrangien(u)
    print('u[%d] = ' %n, u)
    h = np.linalg.solve(-H, g)
    u = u+h
    
    
### Programme linéaire
print('1.3 Programmation linéaire\n')


## Question 11
print('Question 11.\n')   
    
def objectif (v):
    x1, x2, x3, x4, x5, y1, y2, y3, y4, y5 = v
    return ((4000*x1 + 5000*x2 + x3 + x4 + x5)**2 + (8*y1 + 7*y2 + 3*y3 - y4 - y5)**2)


## Question 12
print('Question 12.\n') 

def contraintes (v):
    x1, x2, x3, x4, x5, y1, y2, y3, y4, y5 = v
    return np.array([
            2*x1 + x2 + x3 - 8,
            x1 + 2*x2 +x2 - 7,
            x2 + x5 - 3,
            8*y1 + 7*y2 + 3*y3 + y4 - 4000,
            y1 + 2*y2 +y3 + y5 - 5000,
            x1*y4,
            x2*y5,
            x3*y1,
            x4*y2,
            x5*y3
        ])


## Question 13
print('Question 13.\n') 

print('Il faut introduire autant de multiplicateurs de Lagrange que de contraintes, soit 6.\n')

def L (u):
    v = u[0:10]
    lambdas = u[10:20]
    return(objectif(v) + np.dot(lambdas, contraintes(v)))    
    

## Question 14
print('Question 14.\n') 

u = np.array([3.5, 1.5, 0.5, 0, 0.5, 995, 2005, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], dtype=np.float64)

nabla_L = ag.grad(L)
H_L = ag.hessian(L)

for i in range(4):
     print('u[%d] =' %i, u)
     h = np.linalg.solve(-H_L(u), nabla_L(u))
     u = u + h









    
    