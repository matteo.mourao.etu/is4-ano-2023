import numpy as np
import autograd as ag
import autograd.numpy as np
import math

### Résolution d'un système
print('I-Résolution d un système\n')

## Question 1
print('Question 1.\n')
print('En supperposant les graphes des fonctions f et g (cf 4-graphe_R2_dans_R2.py), nous avons pu constater la présence de deux zéros communs aux deux fonctions:\n')
print('1er zéro : (-2.9, 0.49)\n')
print('2e zéro : (-0.75, 1.8)\n')

## Question 2
print('Question 2.\n')

print('Dans cette question, on implémente la fonction Jac_F telle qu elle renvoie la matrice Jacobienne de F=(f, g).\n')

def f (a,b):
    return b*a**3 - 3*(b-1)*a**2 + b**2 - 1

def g (a,b):
    return (a**2)*(b**2) - 2

def Jac_F (u) :
    a = u[0]
    b = u[1]
    return np.array ([[3*b*a**2 - 6*a*(b-1), a**3 - 3*a**2 + 2*b], [2*a*b**2, 2*b*a**2]], dtype=np.float64)


## Question 3
print('Question 3.\n')

def F (u) :
    a = u[0]
    b = u[1]
    return np.array([f(a,b), g(a,b)])

print('Premier zéro :\n')

u = np.array([-2.9, 0.49], dtype=np.float64)

for n in range (5):
    print('u[%d] =' %n, u)
    h = np.linalg.solve(-Jac_F(u), F(u))  
    u = u + h

print('\nEt cela fournit les valeurs suivantes :')
print('f(u) = %d' %n, f(-2.84235392, 0.49755013))
print('g(u) = %d' %n, g(-2.84235392, 0.49755013))

print('\nDeuxième zéro :\n')

v = np.array([-0.75, 1.8], dtype=np.float64)

for n in range (5):
    print('u[%d] =' %n, v)
    h = np.linalg.solve(-Jac_F(v), F(v))  
    v = v + h

print('\nEt cela fournit les valeurs suivantes :')
print('f(u) = %d' %n, f(-0.77444947, 1.82608887))
print('g(u) = %d' %n, g(-0.77444947, 1.82608887))

print('\nLes résultats obtenus sont cohérents avec les héstimations faites à la question 1.')
      

## Question 4
print('\nQuestion 4.\n')

print('Pour cette question, on remplace la fonction "Jac_f" par la fonction "ag.jacobian(F)".')

J = ag.jacobian(F)

print('Premier zéro :\n')

u = np.array([-2.9, 0.49], dtype=np.float64)

for n in range (5):
    print('u[%d] =' %n, u)
    h = np.linalg.solve(-J(u), F(u))  
    u = u + h

print('\nDeuxième zéro :\n')

v = np.array([-0.75, 1.8], dtype=np.float64)

for n in range (5):
    print('u[%d] =' %n, v)
    h = np.linalg.solve(-J(v), F(v))  
    v = v + h

print('\nNous constatons que nous obtennons bien les mêmes résultats qu à la question précédente.')


## Question 5
print('\nQuestion 5.\n')

print('Pour cette question, on redéfinit la fonction F en utilisant le mode forward\n')

def F(u) :
    a = u[0]
    b = u[1]
    #Pour f(a,b)
    t1 = a**2
    t2 = b**2
    t3 = (a*b - 3*b + 3)*t1 + t2 - 1
    #Pour g(a,b)
    t1 = a**2
    t2 = b**2
    t4 = t1 * t2 - 2
    #Résultat
    return(np.array([t3, t4]))

print('Premier zéro :\n')

u = np.array([-2.9, 0.49], dtype=np.float64)

for n in range (5):
    print('u[%d] =' %n, u)
    h = np.linalg.solve(-J(u), F(u))  
    u = u + h

print('\nDeuxième zéro :\n')

v = np.array([-0.75, 1.8], dtype=np.float64)

for n in range (5):
    print('u[%d] =' %n, v)
    h = np.linalg.solve(-J(v), F(v))  
    v = v + h

print('\nNous constatons que nous obtennons bien les mêmes résultats qu à la question 3.')


