import numpy as np
import autograd as ag
import autograd.numpy as np
import math

### Résolution d'un système
print('I-Analyse des points stationnaires\n')

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5


## Question 1
print('Question 1.\n')
print('On cherche les points stationnaires du graphe, on regarde donc les endroits où le graphe est "plat", c est à dire là où sa dérivée s annulle.\n')
      
print('1er point stationnaire : minimum local (a,b) = (0.3, 1)\n')
print('2e point stationnaire : point col (a,b) = (-0.25, 1.6)\n')
print('3e point stationnaire : maximum local (a,b) = (-1.53, 0.86)\n')
print('4e point stationnaire : point col (a,b) = (-1.43, 0.08)\n')
print('5e point stationnaire : point col (a,b) = (0.52, -1.93)\n')


## Question 2

def nabla_f (a,b):
    da = 3*a**2 + 4*a - 2*b + b**3
    db = -2*a + 2*b + 3*a*b**2 - 2
    return np.array([da, db], dtype = np.float64)

def H_f (a,b):
    daa = 6*a + 4
    dab = -2 + 3*b**2
    dbb = 2 + 6*a*b
    return np.array([[daa, dab], [dab, dbb]], dtype = np.float64)


## Question 3
print('Question 3.\n')

print('Algorithme de Newton : \n')

print('Pour le 1er point stationnaire :\n')
a0 = 0.3
b0 = 1
un = np.array ([a0,b0], dtype=np.float64)
for i in range(4):
    a = un[0]
    b = un[1]
    print('u[%d] =' %i, un, 'f(u[%d]) =' %i, f(a,b))
    H_f_un = H_f(a,b)
    nabla_f_un = nabla_f(a,b)
    h = np.linalg.solve(-H_f_un, nabla_f_un)
    un = un + h

print('\nPour le 2e point stationnaire :\n')
a0 = -0.25
b0 = 1.6
un = np.array ([a0,b0], dtype=np.float64)
for i in range(4):
    a = un[0]
    b = un[1]
    print('u[%d] =' %i, un, 'f(u[%d]) =' %i, f(a,b))
    H_f_un = H_f(a,b)
    nabla_f_un = nabla_f(a,b)
    h = np.linalg.solve(-H_f_un, nabla_f_un)
    un = un + h

print('\nPour le 3e point stationnaire :\n')
a0 = -1.53
b0 = 0.86
un = np.array ([a0,b0], dtype=np.float64)
for i in range(4):
    a = un[0]
    b = un[1]
    print('u[%d] =' %i, un, 'f(u[%d]) =' %i, f(a,b))
    H_f_un = H_f(a,b)
    nabla_f_un = nabla_f(a,b)
    h = np.linalg.solve(-H_f_un, nabla_f_un)
    un = un + h

print('\nPour le 4e point stationnaire :\n')
a0 = -1.43
b0 = 0.08
un = np.array ([a0,b0], dtype=np.float64)
for i in range(4):
    a = un[0]
    b = un[1]
    print('u[%d] =' %i, un, 'f(u[%d]) =' %i, f(a,b))
    H_f_un = H_f(a,b)
    nabla_f_un = nabla_f(a,b)
    h = np.linalg.solve(-H_f_un, nabla_f_un)
    un = un + h

print('\nPour le 5e point stationnaire :\n')
a0 = 0.52
b0 = -1.93
un = np.array ([a0,b0], dtype=np.float64)
for i in range(4):
    a = un[0]
    b = un[1]
    print('u[%d] =' %i, un, 'f(u[%d]) =' %i, f(a,b))
    H_f_un = H_f(a,b)
    nabla_f_un = nabla_f(a,b)
    h = np.linalg.solve(-H_f_un, nabla_f_un)
    un = un + h


## Question 4
print('Question 4.\n')

print('Pour le 1er point : valeurs propres = ', np.linalg.eigvalsh(H_f(0.3, 1)) )
print('Elles sont toutes supérieures à zéro : minimum local\n')

print('Pour le 2e point : valeurs propres = ', np.linalg.eigvalsh(H_f(-0.25, 1.6)) )
print('Elles sont de signe différent : point col\n')

print('Pour le 3e point : valeurs propres = ', np.linalg.eigvalsh(H_f(-1.53, 0.86)) )
print('Elles sont toutes inférieures à zéro : maximum local\n')

print('Pour le 4e point : valeurs propres = ', np.linalg.eigvalsh(H_f(-1.43, 0.08)) )
print('Elles sont de signe différent : point col\n')

print('Pour le 4e point : valeurs propres = ', np.linalg.eigvalsh(H_f(0.52, -1.93)) )
print('Elles sont de signe différent : point col\n')


## Question 5
print('Question 5.\n')

def fbis(u):
    a = u[0]
    b = u[1]
    return a**3 + 2*a**2 - 2*a*b + b**2 + a*b**3 - 2*b + 5

def Newton_f(un):
    for i in range(4):
        print('u[%d] =' %i, un, 'f(u[%d]) =' %i, fbis(un))
        H_f_un = ag.hessian(fbis)
        nabla_f_un = ag.grad(fbis)
        h = np.linalg.solve(-H_f_un(un), nabla_f_un(un))
        un = un + h        

print('\nPour le 5e point stationnaire :\n', Newton_f(np.array([0.3, 1], dtype = np.float64)))


## Question 6
print('Question 6.\n')


def nabla_fter(u):
    a = u[0]
    b = u[1]
    t1 = b ** 2
    t2 = a ** 2
    t3 = a * t1 * b - 2 * a * b + t2 * a - 2 * b + t1 + 2 * t2 + 5
    
    # Utilisez t1, t2, et t3 pour calculer le gradient
    da = 3 * t2 + 4 * a - 2 * b + t3
    db = -2 * a + 2 * b + 3 * a * t1 - 2
    
    return np.array([da, db], dtype=np.float64)

def Newton_fter(un):
    for i in range(4):
        print('u[%d] =' %i, un, 'f(u[%d]) =' %i, fbis(un))
        H_f_un = H_f(un[0], un[1])
        nabla_f_un = nabla_fter(un)
        h = np.linalg.solve(-H_f_un, nabla_f_un)
        un = un + h    

# Pour le premier point stationnaire :
u0 = np.array([0.3, 1], dtype=np.float64)
Newton_fter(u0)











