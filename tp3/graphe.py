#!/bin/python3

import autograd as ag
import autograd.numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt

def f (a,b):
    return a**3 + 2*a**2 - 2*a*b + b**2 + a - 2*b + 5

fig = plt.figure(figsize = (20,20))
ax = plt.axes(projection='3d')

ax.set_xlabel('$a$', labelpad=20)
ax.set_ylabel('$b$', labelpad=20)
ax.set_zlabel('$f(a,b)$', labelpad=20)

aplot = np.arange (-.5, 2, 0.05)
bplot = np.arange (-.5, 3, 0.05)

A, B = np.meshgrid (aplot, bplot)
Z = f(A,B)
Z = np.clip (Z, 3, 8)

ax.plot_surface(A, B, Z, cmap="spring_r", lw=0.5, rstride=1, cstride=1, alpha=0.5)
ax.contour(A, B, Z, 30, colors="k", linestyles="dotted")

plt.show ()

